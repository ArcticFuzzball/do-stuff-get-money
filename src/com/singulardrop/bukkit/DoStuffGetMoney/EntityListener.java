package com.singulardrop.bukkit.DoStuffGetMoney;


import java.rmi.server.Skeleton;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Ghast;
import org.bukkit.entity.Giant;
import org.bukkit.entity.Pig;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.Slime;
import org.bukkit.entity.Spider;
import org.bukkit.entity.Squid;
import org.bukkit.entity.Wolf;
import org.bukkit.entity.Zombie;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.util.config.Configuration;

import com.iCo6.iConomy;
import com.iCo6.system.Accounts;
import com.iCo6.system.Holdings;

@SuppressWarnings("deprecation")
public class EntityListener extends org.bukkit.event.entity.EntityListener {
	
	DoStuffGetMoney pluginInstance;
	
	public EntityListener(DoStuffGetMoney plugin) {
		pluginInstance = plugin;
	}
	
	public void onEntityDeath(EntityDeathEvent event) {
		Entity victim = event.getEntity();
		Player killer = null;
		if (victim.getLastDamageCause() instanceof EntityDamageByEntityEvent) {
			EntityDamageByEntityEvent nEvent = (EntityDamageByEntityEvent)event.getEntity().getLastDamageCause();
			if (nEvent.getDamager() instanceof Player) {
				killer = (Player)nEvent.getDamager();
			} else if (nEvent.getDamager() instanceof Arrow) {
				Arrow arrow = (Arrow)nEvent.getDamager();
				if (arrow.getShooter() instanceof Player) {
					killer = (Player)arrow.getShooter();
				}
			}
			
			if (killer != null) {
				Configuration config = pluginInstance.getConfiguration();
				
				double amount = 0;
				String mobName = "";
				
				if (victim instanceof Chicken) {
					mobName = "Chicken"; 
					amount = config.getDouble("mob-amounts.chicken", 0);
				} else if (victim instanceof Cow) {
					mobName = "Cow"; 
					amount = config.getDouble("mob-amounts.cow", 0);
				} else if (victim instanceof Creeper) {
					mobName = "Creeper"; 
					amount = config.getDouble("mob-amounts.creeper", 0);
				} else if (victim instanceof Ghast) {
					mobName = "Ghast"; 
					amount = config.getDouble("mob-amounts.ghast", 0);
				} else if (victim instanceof Giant) {
					mobName = "Giant"; 
					amount = config.getDouble("mob-amounts.giant", 0);
				} else if (victim instanceof Pig) {
					mobName = "Pig"; 
					amount = config.getDouble("mob-amounts.pig", 0);
				} else if (victim instanceof PigZombie) {
					mobName = "Pig Zombie"; 
					amount = config.getDouble("mob-amounts.pigzombie", 0);
				} else if (victim instanceof Sheep) {
					mobName = "Sheep";
					amount = config.getDouble("mob-amounts.sheep", 0);
				} else if (victim instanceof Skeleton) { // Deprecated? WTF?
					mobName = "Skeleton"; 
					amount = config.getDouble("mob-amounts.skeleton", 0);
				} else if (victim instanceof Slime) {
					mobName = "Slime"; 
					amount = config.getDouble("mob-amounts.slime", 0);
				} else if (victim instanceof Spider) {
					mobName = "Spider"; 
					amount = config.getDouble("mob-amounts.spider", 0);
				} else if (victim instanceof Squid) {
					mobName = "Squis"; 
					amount = config.getDouble("mob-amounts.squid", 0);
				} else if (victim instanceof Wolf) {
					mobName = "Wolf"; 
					amount = config.getDouble("mob-amounts.wolf", 0);
				} else if (victim instanceof Zombie) {
					mobName = "Zombie"; 
					amount = config.getDouble("mob-amounts.zombie", 0);
				} else if (victim instanceof Enderman) {
					mobName = "Enderman"; 
					amount = config.getDouble("mob-amounts.enderman", 0);
				}
				
				if (amount > 0) {
					this.add(killer, amount);
					
					killer.sendMessage(String.format("You got %s for killing a %s!", iConomy.format(amount), mobName));
				} else if (amount < 0) {
					this.add(killer, amount);
					
					killer.sendMessage(String.format("Aww. You shouldn't do that :( You lost %s for killing a %s", iConomy.format(Math.abs(amount)), mobName));
				}
				
				config.save();
			}
		}
	}
	
	public void add(Player player, double amount) {
		Accounts acs = new Accounts();
		Holdings ac = acs.get(player.getName()).getHoldings();
		ac.add(amount);
	}
	
}
