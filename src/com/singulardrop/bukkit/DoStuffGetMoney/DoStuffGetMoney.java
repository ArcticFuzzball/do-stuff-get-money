package com.singulardrop.bukkit.DoStuffGetMoney;

import java.util.List;
import java.util.logging.Logger;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.config.Configuration;
import org.bukkit.event.Event;

public class DoStuffGetMoney extends JavaPlugin {
	
	Logger log;
	PluginManager pm;
	Configuration config;
	BlockListener blockListener;
	EntityListener entityListener;
	double blockBreakAmount;
	List<Double> entityKillAmounts;
	
	public String genOutputString(String str, Object ... args) {
		return String.format("[%s] %s", this.getDescription().getName(), String.format(str, args));
	}
	
	@Override
	public void onEnable() {
		blockListener = new BlockListener(this);
		entityListener = new EntityListener(this);
		log = this.getServer().getLogger();
		pm = this.getServer().getPluginManager();
		pm.registerEvent(Event.Type.BLOCK_BREAK, blockListener, Event.Priority.Normal, this);
		pm.registerEvent(Event.Type.ENTITY_DEATH, entityListener, Event.Priority.Normal, this);
		config = this.getConfiguration();
		blockBreakAmount = config.getDouble("block-break-amount", 0);
		entityKillAmounts = config.getDoubleList("entity-amounts", null);
		log.info(this.genOutputString("Enabled"));
	}
	
	@Override
	public void onDisable() {
		config.setProperty("block-break-amount", blockBreakAmount);
		
		// This... this is horrible.
		config.setProperty("mob-amounts.chicken", config.getDouble("mob-amounts.chicken", 0));
		config.setProperty("mob-amounts.cow", config.getDouble("mob-amounts.cow", 0));
		config.setProperty("mob-amounts.creeper", config.getDouble("mob-amounts.creeper", 0));
		config.setProperty("mob-amounts.ghast", config.getDouble("mob-amounts.ghast", 0));
		config.setProperty("mob-amounts.giant", config.getDouble("mob-amounts.giant", 0));
		config.setProperty("mob-amounts.pig", config.getDouble("mob-amounts.pig", 0));
		config.setProperty("mob-amounts.pigzombie", config.getDouble("mob-amounts.pigzombie", 0));
		config.setProperty("mob-amounts.sheep", config.getDouble("mob-amounts.sheep", 0));
		config.setProperty("mob-amounts.skeleton", config.getDouble("mob-amounts.skeleton", 0));
		config.setProperty("mob-amounts.slime", config.getDouble("mob-amounts.slime", 0));
		config.setProperty("mob-amounts.spider", config.getDouble("mob-amounts.spider", 0));
		config.setProperty("mob-amounts.squid", config.getDouble("mob-amounts.squid", 0));
		config.setProperty("mob-amounts.wolf", config.getDouble("mob-amounts.wolf", 0));
		config.setProperty("mob-amounts.zombie", config.getDouble("mob-amounts.zombie", 0));
		config.setProperty("mob-amounts.enderman", config.getDouble("mob-amounts.enderman", 0));
		
		config.save();
		
		log.info(this.genOutputString("Disabled"));
	}
	
	public double getBlockBreakAmount() {
		return blockBreakAmount;
	}
	
}
