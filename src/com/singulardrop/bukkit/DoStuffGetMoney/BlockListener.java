package com.singulardrop.bukkit.DoStuffGetMoney;

import org.bukkit.event.block.BlockBreakEvent;

import com.iCo6.system.Accounts;
import com.iCo6.system.Holdings;

public class BlockListener extends org.bukkit.event.block.BlockListener {
	
	DoStuffGetMoney pluginInstance;
	
	public BlockListener(DoStuffGetMoney plugin) {
		pluginInstance = plugin;
	}
	
	public void onBlockBreak(BlockBreakEvent event) {
		double amount = pluginInstance.getBlockBreakAmount();
		Accounts acs = new Accounts();
		Holdings ac = acs.get(event.getPlayer().getName()).getHoldings();
		ac.add(amount);
	}
	
}
